var express = require('express');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());

var port = process.env.PORT || 3000;
app.listen(port);


console.log("Api molona escuchando en el puerto " + port);

//registrar ruta
// app.get("/apitechu/v1",
// function(req, res){
//   console.log("GET /apitechu/v1");
//   res.send("Hola desde API TechU");
// }

app.get("/apitechu/v1",
function(req, res){
  console.log("GET /apitechu/v1");
  res.send({"msg" : "hola desde apitechu"});
}
);
app.get("/apitechu/v1/users",
function(req, res){
  console.log("GET /apitechu/v1/users");
  res.sendFile('usuarios.json', {root: __dirname});
}
);

app.post("/apitechu/v1/users",
 function(req,res) {
//    console.log("POST /apitechu/V1/users");
   console.log(req.body);
   console.log(req.body.first_name);
   console.log(req.body.last_name);
   console.log(req.body.country);

   var newUser = {
     "first_name" : req.body.first_name,
     "last_name"  : req.body.last_name,
     "country"    : req.body.country
   };

   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);

 }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
 function(req,res) {
//    console.log("POST /apitechu/V1/users");
   console.log("Parámetros");
   console.log(req.params);

   console.log("Query String");
   console.log(req.query);

   console.log("Body");
   console.log(req.body);

   console.log("Headers");
   console.log(req.headers);

 }
)


app.delete("/apitechu/v1/users/:id",
function(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(req.params.id);
  var users = require('./usuarios.json');
  users.splice(req.params.id - 1, 1);
  writeUserDataToFile(users);
  console.log("usuarios borrado");
  res.send({"msg" : "Usuario borrado"})

}
)
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData =JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
  function(err) {
    var msg = ""

    if (err){
         console.log(err);

      }else{
       //  console.log("Usuario persistido");
        msg = "Usuario persistido";
      }
})
}
